# Symfony Jobeet tutorial
### On Ubuntu and Mac
- Day 1: Starting up the project
    - Install docker
    - Install docker-compose
    - Install git
    - Clone tutorial with command `git clone git@bitbucket.org:koffi-formation/jobeet-tutorial.git`
    - Run `cd jobeet-tutorial`
    - Run command `git checkout jobeet-tutorial/day-1`
    - Now: 
        - Start Dokcer on your machine
        - Start all container with `make up` 
        - Check containers running with `make ps`
        - Go into app container with `make app-bash` 
        - install project dependencies `composer install` 
        - Check php version with `php -v`
        - Check all Symfony commands available with `php bin/console list`
    ```
    make up;
    make ps;
    make app-bash;
    ```
    ```
    composer install;
    php -v;
    php bin/console list;
    ```
    - Go to `http://127.0.0.1:8080` on your browser
    - Run this command for day 2 tutorial:
    ```
    git checkout jobeet-tutorial/day-2`
    ```
  
  
- Day 2: The project
    - Four type of users: 
        - admin (owns and administers the website), 
        - user (visits the site looking for a job), 
        - poster (visits the site to post jobs),
        - affiliate (re-publishes jobs on his website).
    - Front stories:
        - On the homepage, the user sees the latest active jobs.
        - A user can ask for all the jobs in a given category.
        - A user refines the list with some keywords.
        - A user clicks on a job to see more detailed information.
        - A user posts a job.
        - A user applies to become an affiliate.
        - An affiliate retrieves the current active job list.
    - Admin stories:
        - An admin configures the site
        - An admin manages the jobs
        - An admin manages the affiliates
    - Run this command for day 3 tutorial:
    ```
    git checkout jobeet-tutorial/day-3`
    ```
  
  
- Day 3: The Data Model
    - Three tables:
        - Jobs
        - Categories
        - Affiliates
    - The Relational Model:
        - See image data_model.png under `jobeet-tutorial/documents/images`
    - Create database `jt-db-name`:
    ```
    make app-bah
    php bin/console doctrine:database:create --if-not-exists
    ```
    - Creating Entity Classes and create metadata for mapping information with Doctrine annotations:
        - `src/Entity/Category.php`:
        ````php
        namespace App\Entity;
        
        use Doctrine\Common\Collections\ArrayCollection;
        use Doctrine\ORM\Mapping as ORM;
        
        /**
        * @ORM\Entity()
        * @ORM\Table(name="categories")
        */
        class Category
        {
          /**
           * @var int
           *
           * @ORM\Column(type="integer")
           * @ORM\Id
           * @ORM\GeneratedValue(strategy="AUTO")
           */
          private $id;
        
          /**
           * @var string
           *
           * @ORM\Column(type="string", length=100)
           */
          private $name;
        
          /**
           * @var Job[]|ArrayCollection
           *
           * @ORM\OneToMany(targetEntity="Job", mappedBy="category")
           */
          private $jobs;
        
          /**
           * @var Affiliate[]|ArrayCollection
           *
           * @ORM\ManyToMany(targetEntity="Affiliate", mappedBy="categories")
           */
          private $affiliates;
        }
        ````
        - `src/Entity/Job.php`:
        ````php
        namespace App\Entity;
        
        use Doctrine\ORM\Mapping as ORM;
        
        /**
        * @ORM\Entity()
        * @ORM\Table(name="jobs")
        */
        class Job
        {
          /**
           * @var int
           *
           * @ORM\Column(type="integer")
           * @ORM\Id
           * @ORM\GeneratedValue(strategy="AUTO")
           */
          private $id;
        
          /**
           * @var string
           *
           * @ORM\Column(type="string", length=255)
           */
          private $type;
        
          /**
           * @var string
           *
           * @ORM\Column(type="string", length=255)
           */
          private $company;
        
          /**
           * @var string|null
           *
           * @ORM\Column(type="string", length=255, nullable=true)
           */
          private $logo;
        
          /**
           * @var string|null
           *
           * @ORM\Column(type="string", length=255, nullable=true)
           */
          private $url;
        
          /**
           * @var string
           *
           * @ORM\Column(type="string", length=255)
           */
          private $position;
        
          /**
           * @var string
           *
           * @ORM\Column(type="string", length=255)
           */
          private $location;
        
          /**
           * @var string
           *
           * @ORM\Column(type="text")
           */
          private $description;
        
          /**
           * @var string
           *
           * @ORM\Column(type="text")
           */
          private $howToApply;
        
          /**
           * @var string
           *
           * @ORM\Column(type="string", length=255, unique=true)
           */
          private $token;
        
          /**
           * @var bool
           *
           * @ORM\Column(type="boolean")
           */
          private $public;
        
          /**
           * @var bool
           *
           * @ORM\Column(type="boolean")
           */
          private $activated;
        
          /**
           * @var string
           *
           * @ORM\Column(type="string", length=255)
           */
          private $email;
        
          /**
           * @var \DateTime
           *
           * @ORM\Column(type="datetime")
           */
          private $expiresAt;
        
          /**
           * @var \DateTime
           *
           * @ORM\Column(type="datetime")
           */
          private $createdAt;
        
          /**
           * @var \DateTime
           *
           * @ORM\Column(type="datetime")
           */
          private $updatedAt;
        
          /**
           * @var Category
           *
           * @ORM\ManyToOne(targetEntity="Category", inversedBy="jobs")
           * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
           */
          private $category;
        }
        ````
        - `src/Entity/Affiliate.php`:
        ````php
        namespace App\Entity;
           
        use Doctrine\Common\Collections\ArrayCollection;
        use Doctrine\ORM\Mapping as ORM;
        
        /**
        * @ORM\Entity()
        * @ORM\Table(name="affiliates")
        */
        class Affiliate
        {
           /**
            * @var int
            *
            * @ORM\Column(type="integer")
            * @ORM\Id
            * @ORM\GeneratedValue(strategy="AUTO")
            */
           private $id;
        
           /**
            * @var string
            *
            * @ORM\Column(type="string", length=255)
            */
           private $url;
        
           /**
            * @var string
            *
            * @ORM\Column(type="string", length=255)
            */
           private $email;
        
           /**
            * @var string
            *
            * @ORM\Column(type="string", length=255, unique=true)
            */
           private $token;
        
           /**
            * @var bool
            *
            * @ORM\Column(type="boolean")
            */
           private $active;
        
           /**
            * @var \DateTime
            *
            * @ORM\Column(type="datetime")
            */
           private $createdAt;
        
           /**
            * @var Category[]|ArrayCollection
            *
            * @ORM\ManyToMany(targetEntity="Category", inversedBy="affiliates")
            * @ORM\JoinTable(name="affiliates_categories")
            */
           private $categories;
        }
        ````
    - Validate the mapping:
    ```
    make app-bah
    php bin/console doctrine:schema:validate
    ```
    - Add Constructors:
        - `src/Entity/Category.php`:
        ````php
        namespace App\Entity;
           
        use Doctrine\Common\Collections\ArrayCollection;
        use Doctrine\ORM\Mapping as ORM;
       
        /**
         * @ORM\Entity()
         * @ORM\Table(name="categories")
         */
        class Category
        {
            // properties
       
            public function __construct()
            {
                $this->jobs = new ArrayCollection();
                $this->affiliates = new ArrayCollection();
            }
       
            // setters and getters
        }
        ````
        - `src/Entity/Affiliate.php`:
        ```php
        namespace App\Entity;
        
        use Doctrine\Common\Collections\ArrayCollection;
        use Doctrine\ORM\Mapping as ORM;
        
        /**
         * @ORM\Entity()
         * @ORM\Table(name="affiliates")
         */
        class Affiliate
        {
            // properties
        
            public function __construct()
            {
                $this->categories = new ArrayCollection();
            }
        
            // setters and getters
        }
        ```
        - Add Getters and Setters to the entities, you can generate them with your IDE (Example: PhpStorm)
        - Add Lifecycle Callbacks:
            - For job:
            ````php
            /**
             * @ORM\Entity()
             * @ORM\Table(name="jobs")
             * @ORM\HasLifecycleCallbacks()
             */
            class Job
            {
                // properties
            
                // getters/setters
            
                /**
                 * @ORM\PrePersist()
                 */
                public function prePersist()
                {
                    $this->createdAt = new \DateTime();
                    $this->updatedAt = new \DateTime();
                }
            
                /**
                 * @ORM\PreUpdate()
                 */
                public function preUpdate()
                {
                    $this->updatedAt = new \DateTime();
                }
            }
            ```` 
            - For affiliate:
            ````php
            /**
             * @ORM\Entity()
             * @ORM\Table(name="affiliates")
             * @ORM\HasLifecycleCallbacks()
             */
            class Affiliate
            {
                // properties
            
                // contructor
            
                // getters/setters
            
                /**
                 * @ORM\PrePersist
                 */
                public function prePersist()
                {
                    $this->createdAt = new \DateTime();
                }
            }
            ````
        - Create the Database Tables/Schema by generating migration files and executing them:
            ```
            make app-bash
            php bin/console doctrine:migration:diff
            php bin/console doctrine:migration:migrate -n
            ```
        - Create The Initial Data:
            - Install doctrine-fixtures bundle
            ```
            make app-bash
            composer require --dev doctrine/doctrine-fixtures-bundle
            ```
            - Create first data fixtures:
                - `src/DataFixtures/CategoryFixtures.php`:
                ````php
                namespace App\DataFixtures;
                
                use App\Entity\Category;
                use Doctrine\Bundle\FixturesBundle\Fixture;
                use Doctrine\Persistence\ObjectManager;
                
                class CategoryFixtures extends Fixture
                {
                    /**
                     * @param ObjectManager $manager
                     *
                     * @return void
                     */
                    public function load(ObjectManager $manager) : void
                    {
                        $designCategory = new Category();
                        $designCategory->setName('Design');
                
                        $programmingCategory = new Category();
                        $programmingCategory->setName('Programming');
                
                        $managerCategory = new Category();
                        $managerCategory->setName('Manager');
                
                        $administratorCategory = new Category();
                        $administratorCategory->setName('Administrator');
                
                        $manager->persist($designCategory);
                        $manager->persist($programmingCategory);
                        $manager->persist($managerCategory);
                        $manager->persist($administratorCategory);
                
                        $manager->flush();
                
                        $this->addReference('category-design', $designCategory);
                        $this->addReference('category-programming', $programmingCategory);
                        $this->addReference('category-manager', $managerCategory);
                        $this->addReference('category-administrator', $administratorCategory);
                    }
                }
                ````
                - `src/DataFixtures/JobFixtures.php`:
                ````php
                namespace App\DataFixtures;
                
                use App\Entity\Job;
                use Doctrine\Bundle\FixturesBundle\Fixture;
                use Doctrine\Common\DataFixtures\DependentFixtureInterface;
                use Doctrine\Persistence\ObjectManager;
                
                class JobFixtures extends Fixture implements DependentFixtureInterface
                {
                    /**
                     * @param ObjectManager $manager
                     *
                     * @return void
                     */
                    public function load(ObjectManager $manager) : void
                    {
                        $fullTimeJob = new Job();
                        $fullTimeJob->setCategory($manager->merge($this->getReference('category-programming')));
                        $fullTimeJob->setType('full-time');
                        $fullTimeJob->setCompany('Sensio Labs');
                        $fullTimeJob->setLogo('sensio-labs.gif');
                        $fullTimeJob->setUrl('http://www.sensiolabs.com/');
                        $fullTimeJob->setPosition('Web Developer');
                        $fullTimeJob->setLocation('Paris, France');
                        $fullTimeJob->setDescription('You\'ve already developed websites with symfony and you want to work with Open-Source technologies. You have a minimum of 3 years experience in web development with PHP or Java and you wish to participate to development of Web 2.0 sites using the best frameworks available.');
                        $fullTimeJob->setHowToApply('Send your resume to fabien.potencier [at] sensio.com');
                        $fullTimeJob->setPublic(true);
                        $fullTimeJob->setActivated(true);
                        $fullTimeJob->setToken('job_sensio_labs');
                        $fullTimeJob->setEmail('job@example.com');
                        $fullTimeJob->setExpiresAt(new \DateTime('+30 days'));
                
                        $partTimeJob = new Job();
                        $partTimeJob->setCategory($manager->merge($this->getReference('category-design')));
                        $partTimeJob->setType('part-time');
                        $partTimeJob->setCompany('Extreme Sensio');
                        $partTimeJob->setLogo('extreme-sensio.gif');
                        $partTimeJob->setUrl('http://www.extreme-sensio.com/');
                        $partTimeJob->setPosition('Web Designer');
                        $partTimeJob->setLocation('Paris, France');
                        $partTimeJob->setDescription('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in.');
                        $partTimeJob->setHowToApply('Send your resume to fabien.potencier [at] sensio.com');
                        $partTimeJob->setPublic(true);
                        $partTimeJob->setActivated(true);
                        $partTimeJob->setToken('job_extreme_sensio');
                        $partTimeJob->setEmail('job@example.com');
                        $partTimeJob->setExpiresAt(new \DateTime('+30 days'));
                
                        $manager->persist($fullTimeJob);
                        $manager->persist($partTimeJob);
                
                        $manager->flush();
                    }
                
                    /**
                     * @return array
                     */
                    public function getDependencies(): array
                    {
                        return [
                            CategoryFixtures::class,
                        ];
                    }
                }
                ````
                - Execute them:
                ```
                make app-bah
                php bin/console doctrine:fixtures:load -n
                ```
    - Go to your brwoser to see that everything is fine at this time on the project:
    ```
    http://127.0.0.1:8080
    ```
    - Run this command for day 4 tutorial:
    ```
    git checkout jobeet-tutorial/day-4
    ```
  
  
- Day 4: The Controller and the View
    - The MVC Architecture:
        - The Model layer defines the business logic (the database belongs to this layer).
        - The View is what the user interacts with (a template engine is part of this layer).
        - The Controller is a piece of code that calls the Model to get some data that it passes to the View for rendering to the client.
    - Controller:
        - `src/Controller/JobController.php`
        ````php
        namespace App\Controller;
        
        use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
        use Symfony\Component\Routing\Annotation\Route;
        
        /**
         * @Route("job")
         */
        class JobController extends AbstractController
        {
        
        }
        ````
    - Layout:
        - Replace `templates/base.html.twig` content by:
        ````twig
        <!DOCTYPE html>
        <html>
        <head>
            <title>{% block title %}Jobeet - Your best job board{% endblock %}</title>
        
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        
            {% block stylesheets %}{% endblock %}
        
            {% block javascripts %}{% endblock %}
    
            <!-- CSS only -->
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
                      rel="stylesheet"
                      integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
                      crossorigin="anonymous">
        </head>
        <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Jobeet</a>
                </div>
        
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <div>
                                <a href="#" class="btn btn-default navbar-btn">Post a Job</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <div class="container">
            {% block body %}{% endblock %}
        </div>
        </body>
        </html>
        ````
    - Stylesheet:
        - We include `Bootstrap css version 5` on `line 13` in the layout.
    - The Job Homepage Action:
        - `src/Controller/JobController.php` (Include Job and Response classes and complete action method):
        ````php
        use App\Entity\Job;
        use Symfony\Component\HttpFoundation\Response;
        
        class JobController extends AbstractController
        {
            /**
             * Lists all job entities.
             *
             * @Route("/", name="job.list")
             *
             * @return Response
             */
            public function list() : Response
            {
                $jobs = $this->getDoctrine()->getRepository(Job::class)->findAll();
        
                return $this->render('job/list.html.twig', [
                    'jobs' => $jobs,
                ]);
            }
        }
        ````
    - The Job Homepage Template:
        - `templates/job/list.html.twig`
        ````twig
        {% extends 'base.html.twig' %}
        
        {% block body %}
            <table class="table text-center">
                <tr>
                    <th class="active text-center">City</th>
                    <th class="active text-center">Position</th>
                    <th class="active text-center">Company</th>
                </tr>
        
                {% for job in jobs %}
                    <tr>
                        <td>{{ job.location }}</td>
                        <td>
                            <a href="#">
                                {{ job.position }}
                            </a>
                        </td>
                        <td>{{ job.company }}</td>
                    </tr>
                {% endfor %}
            </table>
        {% endblock %}
        ````
    - Twig template:
        - `extends` tag used in `list.html.twig` means that this template extends base template `base.html.twig` 
        and can redefine blocks from it. In our case we define content for body block.
    - The Job Page Action (create action to see one job):
        - `src/Controller/JobController.php`:
        ```php
        /**
         * Finds and displays a job entity.
         *
         * @Route("/{id}", name="job.show")
         *
         * @param Job $job
         *
         * @return Response
         */
        public function show(Job $job) : Response
        {
            return $this->render('job/show.html.twig', [
                'job' => $job,
            ]);
        }
        ```
    - The Job Page Template:
        - `templates/job/show.html.twig`
        ````twig
        {% extends 'base.html.twig' %}
        
        {% block body %}
            <h1>Job</h1>
        
            <div class="media" style="margin-top: 60px;">
                <div class="media-body">
                    <div class="row">
                        <div class="col-sm-10">
                            <h3 class="media-heading"><strong>{{ job.company }}</strong> <i>({{ job.location }})</i></h3>
                        </div>
        
                        <div class="col-sm-2">
                            <i class="pull-right">posted on {{ job.createdat|date('m/d/Y') }}</i>
                        </div>
                    </div>
        
                    <p>
                        <strong>{{ job.position }}</strong>
                        <small> - <i>{{ job.type }}</i></small>
                    </p>
        
                    <p>{{ job.description|nl2br }}</p>
        
                    <p style="margin-top: 40px;">
                        <strong>How to apply?</strong>
                    </p>
        
                    <p>{{ job.howToApply }}</p>
        
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <a class="btn btn-default" href="#">
                                <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                                Back to list
                            </a>
        
                            <a class="btn btn-primary" href="#">
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                Edit
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        {% endblock %}
        ````
    - Run next commands to see all routes in project:
    ````
    make app-bash
    php bin/console debug:route
    ````
    - Run next commands to use url rewrite in project:
    ````
    make app-bash
    composer require symfony/apache-pack
    ````
    - Change `DATABASE_URL` value in .env:
    ```
    DATABASE_URL="postgresql://jt-db-user:jt-db-password@database:5432/jt-db-name?serverVersion=13&charset=utf8"
    ```
    - Go to those urls in your browser:
        - `http://127.0.0.1:8080/job/`
        - `http://127.0.0.1:8080/job/1`
        - `http://127.0.0.1:8080/job/2`
    - Run this command for day 5 tutorial:
    ```
    git checkout jobeet-tutorial/day-5
    ```
  

- Day 5:
    - First links:
        - Add links into 
            - `templates/job/list.html.twig`:
            ````twig
            - <a href="#">
            + <a href="{{ path('job.show', {id: job.id}) }}">
            ````
            - `templates/job/show.html.twig`:
            ````twig
            - <a class="btn btn-default" href="#">
            + <a class="btn btn-default" href="{{ path('job.list') }}">
                  <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                  Back to list
              </a>
            ````
            - `templates/base.html.twig`
            ````twig
            - <a class="navbar-brand" href="#">Jobeet</a>
            + <a class="navbar-brand" href="{{ path('job.list') }}">Jobeet</a>
            ````
    - Check Routing Configuration in `config/routes/annotations.yaml` and annotation in `src/Controller/JobController.php`.
    - Check Routing Configuration in Dev Environment in `config/routes/dev/web_profiler.yaml` and `config/routes/dev/framework.yaml`:
    - Route Customizations:
        - Remove route configuration `@Route("job") at the beginning `of the controller `src/Controller/JobController`
        - Modify route configuration on action `show(Job $job)` to `@Route("job/{id}", name="job.show")`
        - Now, if you open `http://127.0.0.1/` in your browser, youÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ll see the Job homepage
    - Route Requirements:
        - Add a regular expression and allowed method optoon on route requirement of `show(Job $job)` in the controller 
        `src/Controller/JobController`:
            - @Route("job/{id}", name="job.show", methods="GET", requirements={"id" = "\d+"})
    - Route Debugging:
    ```
    make app-bash
    php bin/console debug:router
    php bin/console debug:router job.show
    ```
    - Run this command for day 6 tutorial:
    ```
    git checkout jobeet-tutorial/day-6
    ```


- Day 6: More with the Entity
	- Get active jobs that was posted less than 30 days ago with Doctrine Query Builder, change list method action content  in `src/Controller/JobController.php`
	````php
	
	use Doctrine\ORM\EntityManagerInterface;

	class JobController extends AbstractController
	{
		//...
		public function list(EntityManagerInterface $em) : Response
		{
			$query = $em->createQuery(
				'SELECT j FROM App:Job j WHERE j.createdAt > :date'
			)->setParameter('date', new \DateTime('-30 days'));
			$jobs = $query->getResult();

			return $this->render('job/list.html.twig', [
				'jobs' => $jobs,
			]);
		}

		//...
	}
	````
	- Debugging Doctrine generated SQL with Symfony web debug toolbar
	- Do more lifecycle callbacks by using `expired_at` Job entity field in list method action in JobController, 
		- The goal is to set expired_at value when creating a job and uing this field on the query to retrieve active job:
		- In `src/Entity/Job.php`, modify `prepersist()` method:
		````php
		/**
		 * @ORM\PrePersist()
		 */
		public function prePersist()
		{
			$this->createdAt = new \DateTime();
			$this->updatedAt = new \DateTime();

			if (!$this->expiresAt) {
				$this->expiresAt = (clone $this->createdAt)->modify('+30 days');
			}
		}
		````
		- `src/Controller/JobController.php`:
		````php
		public function list(EntityManagerInterface $em) : Response
		{
			$query = $em->createQuery(
				'SELECT j FROM App:Job j WHERE j.expiresAt > :date'
			)->setParameter('date', new \DateTime());
			$jobs = $query->getResult();

			return $this->render('job/list.html.twig', [
				'jobs' => $jobs,
			]);
		}
		````
	- Add an expired job in Job Data Fixture in `src/DataFixtures/JobFixtures.php`:
	````php
	$jobExpired = new Job();
	$jobExpired->setCategory($manager->merge($this->getReference('category-programming')));
	$jobExpired->setType('full-time');
	$jobExpired->setCompany('Sensio Labs');
	$jobExpired->setLogo('sensio-labs.gif');
	$jobExpired->setUrl('http://www.sensiolabs.com/');
	$jobExpired->setPosition('Web Developer Expired');
	$jobExpired->setLocation('Paris, France');
	$jobExpired->setDescription('Lorem ipsum dolor sit amet, consectetur adipisicing elit.');
	$jobExpired->setHowToApply('Send your resume to lorem.ipsum [at] dolor.sit');
	$jobExpired->setPublic(true);
	$jobExpired->setActivated(true);
	$jobExpired->setToken('job_expired');
	$jobExpired->setEmail('job@example.com');
	$jobExpired->setExpiresAt(new \DateTime('-10 days'));

	$manager->persist($jobExpired);
	````
	- Refactoring
	


		